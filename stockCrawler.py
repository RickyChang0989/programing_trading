import requests
from io import StringIO
import pandas as pd
import numpy as np
import datetime

now = datetime.datetime.now()

print("currentYear: %d" % now.year)
print("currentMonth: %02d" % now.month)
print("currentDay: %d" % now.day)

datestr = "%d%02d%02d" % (now.year, now.month, now.day)
url = 'http://www.twse.com.tw/exchangeReport/MI_INDEX?response=csv&date=' + datestr + '&type=ALL'
r = requests.post(url)
df = pd.read_csv(StringIO("\n".join([i.translate({ord(c): None for c in ' '}) 
                                     for i in r.text.split('\n') 
                                     if len(i.split('",')) == 17 and i[0] != '='])), header=0)
                                     
df[pd.to_numeric(df['本益比'], errors='coerce') < 15]                                  