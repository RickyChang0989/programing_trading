# -*- coding: UTF-8 -*-

#定義計算商品
Product='TX'
Period='201803'

#定義開盤日期、開盤、收盤時間
Date = '20180314'
Otime ='84500'
Ctime = '134500'

#取得資料，依照逗點分隔，依照分隔符號分解欄位，去除空白
I020 = [line.replace(' ','').split(",") for line in open('Daily_2018_03_14.csv')][1:]

#取出指定商品
ProdData = [ line for line in I020 if line[1]==Product and line[2]==Period and line[0]==Date and int(line[3])>= int(Otime) and int(line[3])<= int(Ctime) ]

#定義變數初始值
lastPrice=int(ProdData[0][4])
outDesk=0
inDesk=0

#開始計算內外盤
for i in ProdData[1:]:
 price = int(i[4])
 qty = int(i[5])
 if price > lastPrice:
  outDesk+=qty
 if price < lastPrice:
  inDesk+=qty
 lastPrice = price
 print "Time:",i[0]," Price:",price," OutDesk:",outDesk," InDesk:",inDesk
