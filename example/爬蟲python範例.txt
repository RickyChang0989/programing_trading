import urllib.request,re

def float_or_na(value):
    return float(value) if value != 'N/A' else None

x = urllib.request.urlopen("https://www.moneydj.com/funddj/yb/YP302000.djhtm?a=ET000001").read().decode("big5") 
y=x.split('<tbody>')[1]
z=y.split('</tbody>')[0]
a=z.split('<tr>')[1:]

resource=[]

for i in a :
 k=[ re.sub('<td .{1,} >','',j) for j in i.replace('\r\n','').split('</td>')[:-1]]
 resource+=[[ re.sub('.{1,}>','',l.strip('</a>')) for l in k]]

resource= [ [i[0],i[1],i[2],float_or_na(i[3]),i[4],float_or_na(i[5]),float_or_na(i[6]),float_or_na(i[7]),float_or_na(i[8]),float_or_na(i[9]),float_or_na(i[10])] for i in resource]

sorted(resource, key=lambda x: (x[5] is None, x[5]))

