# -*- coding: UTF-8 -*-

#定義計算商品
Product='TX'
Period='201803'

#定義開盤日期、開盤、收盤時間
Date = '20180314'
Otime ='84500'
Ctime = '134500'

#取得資料，依照逗點分隔，依照分隔符號分解欄位，去除空白
I020 = [line.replace(' ','').split(",") for line in open('Daily_2018_03_14.csv')][1:]

#取出指定商品
ProdData = [ line for line in I020 if line[1]==Product and line[2]==Period and line[0]==Date and int(line[3])>= int(Otime) and int(line[3])<= int(Ctime) ]

#設定進出場時機點
OrderTime = 90000
CoverTime = 110000
OrderPrice = 0
CoverPrice = 0

#開始進行進場判斷
for i in ProdData:
 time=int(i[3])
 price=int(i[4])
 if time > OrderTime:
  OrderPrice=price
  break

ProdData2 = [ line for line in ProdData if int(line[3])>= int(OrderTime) ]

#開始進行出場判斷
for i in ProdData2:
 time=int(i[3])
 price=int(i[4])
 if time > CoverTime:
  CoverPrice=price
  break

print 'OrderTime:' ,OrderTime,'OrderPrice:' ,OrderPrice ,'CoverTime:' ,CoverTime,'CoverPrice:' ,CoverPrice ,'Profit:',CoverPrice-OrderPrice
