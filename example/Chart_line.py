# -*- coding: UTF-8 -*-

#���J�����M��Ψ��
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime

#�w�q�p��ӫ~
Product='TX'
Period='201803'

#�w�q�}�L����B�}�L�B���L�ɶ�
Date = '20180314'
Otime ='84500'
Ctime = '134500'

#���o��ơA�̷ӳr�I���j�A�̷Ӥ��j�Ÿ��������A�h���ť�
I020 = [line.replace(' ','').split(",") for line in open('Daily_2018_03_14.csv')][1:]

#���X���w�ӫ~
ProdData = [ line for line in I020 if line[1]==Product and line[2]==Period and line[0]==Date and int(line[3])>= int(Otime) and int(line[3])<= int(Ctime) ]

#���o�ഫ�ɶ��r��ܮɶ��榡
Time = [ datetime.datetime.strptime(line[3],"%H%M%S%f") for line in ProdData ]
#�Ndatetime�ɶ��榡�ഫ��ø�ϱM�Ϊ��ɶ��榡�A�z�Lmdates.date2num���
Time1 = [ mdates.date2num(line) for line in Time ]
#����Ѧr����ƭ�
Price = [ int(line[4]) for line in ProdData ]

#�w�q�Ϫ���
ax = plt.figure(1) 		#�Ĥ@�i�Ϥ�              
ax = plt.subplot(111)	#�ӱi�Ϥ��Ȥ@�ӹϮ�
#�H�W���A�i²�g�p�U�@��
#fig,ax = plt.subplots()

#ø�s�Ϯ�
#plot_date(X�b����, Y�b����, �u����)
ax.plot_date(Time1, Price, 'k-')

#�w�qtitle
plt.title('Price Line')

#�w�qx�b
hfmt = mdates.DateFormatter('%H:%M:%S')
ax.xaxis.set_major_formatter(hfmt)


#���ø�s�Ϫ�
plt.show()
