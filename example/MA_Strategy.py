# -*- coding: UTF-8 -*-

#定義計算商品
Product='TX'
Period='201803'

#定義開盤日期、開盤、收盤時間
Date = '20180314'
Otime ='84500'
Ctime = '134500'

#取得資料，依照逗點分隔，依照分隔符號分解欄位，去除空白
I020 = [line.replace(' ','').split(",") for line in open('Daily_2018_03_14.csv')][1:]

#取出指定商品
ProdData = [ line for line in I020 if line[1]==Product and line[2]==Period and line[0]==Date and int(line[3])>= int(Otime) and int(line[3])<= int(Ctime) ]

#定義時間轉數值函數
def TimetoNumber(time):
 time=time.zfill(6)
 sec=int(time[:2])*3600+int(time[2:4])*60+int(time[4:6])
 return sec

#定義相關變數
MAarray = []
MA = []
MAValue = 0
lastMAValue =0
lastPrice =0 
STime = TimetoNumber('84500')
Cycle = 60
MAlen = 15

#定義新倉平倉變數
Index=0
OrderTime=0
OrderPrice=0
CoverTime=0
CoverPrice=0
StopLoss=10
TakeProfit=10

#開始進行MA計算
for i in ProdData:
 time=i[3]
 price=int(i[4])
 if len(MAarray)==0:
  MAarray+=[price]
 else:
  if TimetoNumber(time)<STime+Cycle:
   MAarray[-1]=price
  else:
   if len(MAarray)==MAlen:
    MAarray=MAarray[1:]+[price]
   else:
    MAarray+=[price]   
   STime = STime+Cycle
 if len(MAarray)==MAlen:
  MAValue=float(sum(MAarray))/len(MAarray)
  if lastMAValue ==0 :
   lastMAValue = MAValue
   lastPrice = price
   continue
  if Index==0:
   if price > MAValue and lastMAValue < lastPrice:
    Index=1
    OrderTime=time
    OrderPrice=price
   elif price < MAValue and lastMAValue > lastPrice:
    Index=-1
    OrderTime=time
    OrderPrice=price
  elif Index!=0:
   if Index==1:
    if price-OrderPrice>=TakeProfit or OrderPrice-price >= StopLoss:
     Index=0
     CoverTime=time
     CoverPrice=price
     print 'Buy OrderTime:' ,OrderTime,'OrderPrice:' ,OrderPrice ,'CoverTime:' ,CoverTime,'CoverPrice:' ,CoverPrice ,'Profit:',CoverPrice-OrderPrice
     break
   elif Index==-1:
    if price-OrderPrice>=StopLoss or OrderPrice-price >=TakeProfit :
     Index=0
     CoverTime=time
     CoverPrice=price
     print 'Sell OrderTime:' ,OrderTime,'OrderPrice:' ,OrderPrice ,'CoverTime:' ,CoverTime,'CoverPrice:' ,CoverPrice ,'Profit:',OrderPrice-CoverPrice
     break
  lastMAValue = MAValue
  lastPrice = price




