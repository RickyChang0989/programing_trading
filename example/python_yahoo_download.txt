from pandas_datareader import data as pdr
import fix_yahoo_finance as yf
import datetime

yf.pdr_override() 

stocks = ["AAPL"]
start = datetime.datetime(2012,5,31)
end = datetime.datetime(2018,3,1)

f = pdr.get_data_yahoo(stocks, start=start, end=end)