# -*- coding: UTF-8 -*-
import sys

#定義計算商品
Product='TX'
Period='201803'

#定義開盤日期、開盤、收盤時間
Date = '20180314'
Otime ='84500'
Ctime = '134500'

#取得資料，依照逗點分隔，依照分隔符號分解欄位，去除空白
I020 = [line.replace(' ','').split(",") for line in open('Daily_2018_03_14.csv')][1:]

#取出指定商品
ProdData = [ line for line in I020 if line[1]==Product and line[2]==Period and line[0]==Date and int(line[3])>= int(Otime) and int(line[3])<= int(Ctime) ]

#設定進出場變數
TrandTime = 90000
EndTime = 120000
OrderTime = 0
CoverTime = 0
OrderPrice = 0
CoverPrice = 0
Index=0
MaxPrice = 0
MinPrice = 10000000
Spread = 0

#設定停損停利點
StopLoss = 10
TakeProfit = 10

#開始進行高低點判斷
for i in ProdData:
 time=int(i[3])
 price=int(i[4])
 if price > MaxPrice :
  MaxPrice = price
 elif MinPrice > price :
  MinPrice = price
 if time > TrandTime:
  Spread = MaxPrice -MinPrice 
  break

print 'MaxPrice' ,MaxPrice ,'MinPrice',MinPrice ,'Spread',Spread 

ProdData2 = [ line for line in ProdData if int(line[3])>= int(TrandTime) ]

#開始進行進場判斷
for i in ProdData2:
 time=int(i[3])
 price=int(i[4])
 if price > MaxPrice+Spread*0.5 :
  Index=1
  OrderTime =time 
  OrderPrice = price
  break
 if price < MinPrice-Spread*0.5 :
  Index=-1
  OrderTime =time 
  OrderPrice = price
  break
 if time>EndTime :
  print "No Order!"
  sys.exit()

ProdData3 = [ line for line in ProdData2 if int(line[3])>= int(OrderTime) ]

#開始進行出場判斷
for i in ProdData3:
 time=int(i[3])
 price=int(i[4])
 if Index==1:
  if price-OrderPrice >=TakeProfit or OrderPrice-price >= StopLoss:
   CoverTime =time 
   CoverPrice=price
   print 'Buy OrderTime:' ,OrderTime,'OrderPrice:' ,OrderPrice ,'CoverTime:' ,CoverTime,'CoverPrice:' ,CoverPrice ,'Profit:',CoverPrice-OrderPrice
   break
  if time > EndTime:
   CoverTime =time 
   CoverPrice=price
   print 'Buy OrderTime:' ,OrderTime,'OrderPrice:' ,OrderPrice ,'CoverTime:' ,CoverTime,'CoverPrice:' ,CoverPrice ,'Profit:',CoverPrice-OrderPrice
   break
 elif Index== -1 :
  if OrderPrice-price >= TakeProfit or price-OrderPrice>=StopLoss :
   CoverTime =time 
   CoverPrice=price
   print 'Sell OrderTime:' ,OrderTime,'OrderPrice:' ,OrderPrice ,'CoverTime:' ,CoverTime,'CoverPrice:' ,CoverPrice ,'Profit:',OrderPrice-CoverPrice
   break
  if time > EndTime:
   CoverTime =time 
   CoverPrice=price
   print 'Sell OrderTime:' ,OrderTime,'OrderPrice:' ,OrderPrice ,'CoverTime:' ,CoverTime,'CoverPrice:' ,CoverPrice ,'Profit:',OrderPrice-CoverPrice
   break


